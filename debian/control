Source: xcb-util-wm
Section: libdevel
Priority: optional
Maintainer: Debian X Strike Force <debian-x@lists.debian.org>
Uploaders: Arnaud Fontaine <arnau@debian.org>
Build-Depends: debhelper-compat (= 13),
               libxcb1-dev (>= 1.6),
               pkgconf,
               xutils-dev (>= 1:7.7~1~)
Standards-Version: 4.5.1
Homepage: https://xcb.freedesktop.org
Vcs-Git: https://salsa.debian.org/xorg-team/lib/xcb-util-wm.git
Vcs-Browser: https://salsa.debian.org/xorg-team/lib/xcb-util-wm

Package: libxcb-icccm4
Section: libs
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: utility libraries for X C Binding -- icccm
 This package contains the library files needed to run software using
 libxcb-icccm, providing client and window-manager helpers for ICCCM.
 .
 The xcb-util module provides a number of libraries which sit on top of
 libxcb, the core X protocol library, and some of the extension
 libraries. These experimental libraries provide convenience functions
 and interfaces which make the raw X protocol more usable. Some of the
 libraries also provide client-side code which is not strictly part of
 the X protocol but which have traditionally been provided by Xlib.

Package: libxcb-icccm4-dev
Conflicts: libxcb-icccm1-dev
Replaces: libxcb-icccm1-dev
Architecture: any
Multi-Arch: same
Depends: libxcb-icccm4 (= ${binary:Version}),
         libxcb1-dev,
         ${misc:Depends}
Description: utility libraries for X C Binding -- icccm, development files
 This package contains the header and library files needed to build software
 using libxcb-icccm, providing client and window-manager helpers for ICCCM.
 .
 The xcb-util module provides a number of libraries which sit on top of
 libxcb, the core X protocol library, and some of the extension
 libraries. These experimental libraries provide convenience functions
 and interfaces which make the raw X protocol more usable. Some of the
 libraries also provide client-side code which is not strictly part of
 the X protocol but which have traditionally been provided by Xlib.

Package: libxcb-ewmh2
Section: libs
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: utility libraries for X C Binding -- ewmh
 This package contains the library files needed to run software using
 libxcb-ewmh, providing client and window-manager helpers for EWMH.
 .
 The xcb-util module provides a number of libraries which sit on top of
 libxcb, the core X protocol library, and some of the extension
 libraries. These experimental libraries provide convenience functions
 and interfaces which make the raw X protocol more usable. Some of the
 libraries also provide client-side code which is not strictly part of
 the X protocol but which have traditionally been provided by Xlib.

Package: libxcb-ewmh-dev
Architecture: any
Multi-Arch: same
Conflicts: libxcb-ewmh1-dev
Replaces: libxcb-ewmh1-dev
Depends: libxcb-ewmh2 (= ${binary:Version}),
         libxcb1-dev,
         ${misc:Depends}
Description: utility libraries for X C Binding -- ewmh, development files
 This package contains the header and library files needed to build software
 using libxcb-ewmh, providing client and window-manager helpers for EWMH.
 .
 The xcb-util module provides a number of libraries which sit on top of
 libxcb, the core X protocol library, and some of the extension
 libraries. These experimental libraries provide convenience functions
 and interfaces which make the raw X protocol more usable. Some of the
 libraries also provide client-side code which is not strictly part of
 the X protocol but which have traditionally been provided by Xlib.
